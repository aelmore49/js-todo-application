import uuidv4 from "uuid/v4";


// Setup the empty todos array
let todos = [];

// Read existing notes from localStorage
const loadTodos = () => {
	const todosJSON = localStorage.getItem("todos");
	console.log(typeof todosJSON)
	if (todosJSON) {
		todos = JSON.parse(todosJSON);
	} else {
		todos = [];
	}
};

// save todos
const saveTodos = function () {
	localStorage.setItem("todos", JSON.stringify(todos));
};

// retrieve current todos
const getTodos = () => {
	return todos;
};

// create a new todo when text gets entered 
const createTodo = (text) => {
	const id = uuidv4();
	todos.push({
		id: id,
		text,
		completed: false
	});
	saveTodos();
};

// remove a todo when remove button is clicked
const removeTodo = (id) => {
	const todoIndex = todos.findIndex((todo) => {
		return todo.id === id;
	});
	if (todoIndex > -1) {
		todos.splice(todoIndex, 1);
		saveTodos();
	}
};

// toggle todos when checked and unchecked
const toggleTodo = (id) => {
	const todo = todos.find((todo) => {
		return todo.id === id;
	});
	if (todo) {
		todo.completed = !todo.completed;
		saveTodos();
	}
};

// Make sure to call loadTodos and setup the exports
loadTodos();

export {
	getTodos,
	createTodo,
	removeTodo,
	toggleTodo,
	loadTodos
};