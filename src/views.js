import {
    getTodos
} from "./todos";
import {
    getFilters
} from "./filters";

import {
    toggleTodo,
    removeTodo
} from "./todos"

// Render application todos based on filters
const renderTodos = () => {
    const todoEl = document.querySelector('#todos');
    const {
        searchText,
        hideCompleted
    } = getFilters();
    const filteredTodos = getTodos().filter(function (todo) {
        const searchTextMatch = todo.text.toLowerCase().includes(searchText.toLowerCase());
        const hideCompletedMatch = !hideCompleted || !todo.completed;
        return searchTextMatch && hideCompletedMatch
    })
    const incompleteTodos = filteredTodos.filter(function (todo) {
        return !todo.completed
    });
    todoEl.innerHTML = '';
    todoEl.appendChild(generateSummaryDom(incompleteTodos));
    if (filteredTodos.length > 0) {
        filteredTodos.forEach(function (todo) {
            todoEl.appendChild(generateTodoDOM(todo))
        })
    } else {
        const messageEl = document.createElement('p');
        messageEl.classList.add('empty-message');
        messageEl.textContent = 'No to-dos to show';
        todoEl.appendChild(messageEl);
    }
}

// Get the DOM element for an individual note
const generateTodoDOM = function (todo) {
    const todoEl = document.createElement('label');
    const containerEl = document.createElement('div')
    const checkbox = document.createElement('input');
    const todoText = document.createElement('span');
    const removeButton = document.createElement('button');
    // Setup todo checkbox
    checkbox.setAttribute('type', 'checkbox');
    checkbox.checked = todo.completed;
    containerEl.appendChild(checkbox);
    checkbox.addEventListener('change', function () {
        toggleTodo(todo.id)
        renderTodos();
    })
    // Setup todo text
    todoText.textContent = todo.text;
    containerEl.appendChild(todoText);
    // Setup container
    todoEl.classList.add('list-item');
    containerEl.classList.add('list-item__container');
    todoEl.appendChild(containerEl)
    // Setup removeButton 
    removeButton.textContent = 'remove';
    removeButton.classList.add('button', 'button--text');
    todoEl.appendChild(removeButton);
    removeButton.addEventListener('click', () => {
        removeTodo(todo.id);
        renderTodos();
    })
    return todoEl;
}
// Get the DOM elements for list summary 
const generateSummaryDom = function (incompleteTodos) {
    const summary = document.createElement('h2');
    summary.classList.add('list-title');
    const plural = incompleteTodos.length === 1 ? '' : 's';
    summary.textContent = `You have ${incompleteTodos.length} todo${plural} left.`
    return summary;
}

// Make sure to set up the exports
export {
    renderTodos,
    generateTodoDOM,
    generateSummaryDom
}